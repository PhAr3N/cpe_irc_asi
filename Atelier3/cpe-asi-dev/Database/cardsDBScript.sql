CREATE TABLE IF NOT EXISTS public.transaction
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    "userId" integer,
    "cardId" integer,
    "cardPrice" double precision,
    "transacDate" "char"[]
)