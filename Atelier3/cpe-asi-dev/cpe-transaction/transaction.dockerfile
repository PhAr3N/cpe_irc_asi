FROM maven:3.8.4-openjdk-11 AS transaction-build
WORKDIR /opt/Transaction
COPY ./pom.xml .
COPY ./src/ ./src/
# RUN mvn package -DskipTests
COPY ./ci_settings.xml .
RUN mvn package -s ci_settings.xml -DskipTests

FROM openjdk:11.0-jre
WORKDIR /opt/Transaction
COPY --from=transaction-build /opt/Transaction/target/*.jar transaction.jar
ENTRYPOINT java -jar transaction.jar
