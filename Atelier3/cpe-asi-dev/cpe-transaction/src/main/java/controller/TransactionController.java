package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dto.MarketDTO;
import pojo.card.Card;
import pojo.transaction.Transaction;
import rest.MarketRest;
import rest.TransactionRest;
import service.TransactionService;

@CrossOrigin
@RestController
public class TransactionController{
	

	@Autowired
	TransactionService transactionService;

	@RequestMapping("/api/transaction/get/{userId}")
	public List<Transaction> getTransactionsByUser(int userId) {
		return transactionService.getTransactionsByUser(userId);
	}
	
	@RequestMapping("/api/transaction/add/{userId}/{cardId}/{cardPrice}/{transacDate}")
    public void addTransaction(int userId, int cardId,  Double cardPrice, String transacDate) {
	    transactionService.addTransaction(userId, cardId, cardPrice, transacDate);
    }
	 
}