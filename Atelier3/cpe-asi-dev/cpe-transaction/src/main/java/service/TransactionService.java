package service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import repository.TransactionRepository;
import pojo.transaction.Transaction;

@Service
public class TransactionService {
	
	private RestTemplate restTemplate = new RestTemplate();
	
	private TransactionRepository transactionRepository;

	public List<Transaction> getTransactionsByUser(int userId) {

		Optional<List<Transaction>> transactions = transactionRepository.getTransactionsByUser(userId);
    	
    	if(transactions.isPresent()) {
    		return transactions.get();
    	}
    	return null;
	}

	public void addTransaction(int userId, int cardId, Double cardPrice, String transacDate) {
		transactionRepository.AddTransaction(userId, cardId, cardPrice, transacDate);
	}
}