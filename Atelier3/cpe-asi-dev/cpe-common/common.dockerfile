FROM maven:3.8.4-openjdk-11 AS common-build
WORKDIR /opt/common
COPY ./pom.xml .
COPY ./src/ ./src/
# RUN mvn package -DskipTests
RUN mvn package -DskipTests

FROM openjdk:11.0-jre
WORKDIR /opt/common
COPY --from=common-build /opt/common/target/*.jar common.jar
ENTRYPOINT java -jar common.jar
